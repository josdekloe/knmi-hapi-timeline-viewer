# KNMI HAPI timeline viewer

The KNMI HAPI timeline viewer is a web-based application for visualizing timeseries data. It makes use of the [Heliophysics Application Programming Interface (HAPI) specification](http://www.hapi-server.org). It is designed to work with data from any of the public [HAPI servers](http://www.hapi-server.org). The features of the viewer are aimed at showing a clean and crisp display of information, highlighting relationships between various data sources, and providing interactivity through smooth zooming and panning on time series data.

This is a companion product to [KNMI's HAPI server](https://gitlab.com/KNMI-OSS/spaceweather/knmi-hapi-server), which contains features to make it easier to host datasets that allow for zooming over a wide range of timespans. These two projects replace [an older implementation of both server and viewer](https://gitlab.com/KNMI-OSS/spaceweather/sw_timeline_viewer).

## Requirements

-   nodejs and npm. nodejs v19.4.0 and npm 9.2.0 were used in development, but any relevantly recent versions will probably work. These tools can be easily installed using Node Version Manager: https://github.com/nvm-sh/nvm
-   Further javascript requirements as listed in `package.json`

## Quick start guide

Take the following steps to configure the viewer on your system:

-   Clone this repository and move into the project directory.
-   Install the dependencies: `npm install`
-   Compile the svelte code into javascript using `npm run build`. This will result in up-to-date files in the `dist` directory, which are the static HTML/CSS/JS files that can be served from any webserver. If the project is not run under the root of your server, you might have to change the two links in `static/index.html` that point to the javascript and CSS files into relative URLs, by simply removing the leading backslash.
-   Alternatively, for developers, you can use `npm run dev` to start a live development server. Running this command will compile the code, start a node-based server on your local machine and give you the address of that server. Pressing the 'O' key will open this address in your browser. In addition, whenever you save any changes to your code in a text editor, the code is conveniently automatically recompiled in the background, and the running code is updated.

## Acknowledgements

The code has been maintained and expanded with support from ESA through the Swarm Data Innovation and Science Cluster (Swarm DISC). For more information on Swarm DISC, please visit https://earth.esa.int/eogateway/activities/swarm-disc
