{
    "options": {
        "fill_vertical": true,
        "fill_horizontal": true,
        "zoom_detail_factor": 1
    },
    "default_zoom": {
        "tmin": "-PT3H",
        "tmax": "PT1H"
    },
    "global_margins": {
        "top": 0,
        "bottom": 0,
        "left": 65,
        "right": 30
    },
    "subplots": [
        {
            "title": "SDO HMI IF",
            "title_color": "#ffffff",
            "visible": true,
            "legend_visible": false,
            "ydomain": {
                "default": [0, 1],
                "bounds": ["0", "1"]
            },
            "y_label": "",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "hidden",
            "height": 250,
            "annot_options": {
                "annot_background_colors": [
                    {
                        "y0": 0,
                        "y1": 1,
                        "color": "#000000",
                        "opacity": 1
                    }
                ]
            },
            "margins": {
                "top": 30,
                "bottom": 30
            },
            "plot_elements": [
                {
                    "title": "SDO HMIIF",
                    "visible": true,
                    "cadence": "PT2H",
                    "plot_options": {
                        "plot_type": "url_template_epoch_images",
                        "url_template": "https://sdo.gsfc.nasa.gov/assets/img/browse/{YYYY}/{MM}/{DD}/{YYYYMMDD}_{HHmmss}_256_HMIIF.jpg",
                        "epoch_offset_start_of_day": "PT0M",
                        "time_substitutions": [
                            {
                                "placeholder": "{YYYY}",
                                "format": "YYYY",
                                "epoch_offset": "PT0M"
                            },
                            {
                                "placeholder": "{MM}",
                                "format": "MM",
                                "epoch_offset": "PT0M"
                            },
                            {
                                "placeholder": "{DD}",
                                "format": "DD",
                                "epoch_offset": "PT0M"
                            },
                            {
                                "placeholder": "{YYYYMMDD}",
                                "format": "YYYYMMDD",
                                "epoch_offset": "PT0M"
                            },
                            {
                                "placeholder": "{HHmmss}",
                                "format": "HHmmss",
                                "epoch_offset": "PT0M"
                            }
                        ],
                        "epoch_indicator_color": "yellow"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "",
                        "parameter": "",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {}
                }
            ]
        },
        {
            "title": "SDO HMI B",
            "title_color": "#ffffff",
            "visible": true,
            "legend_visible": false,
            "ydomain": {
                "default": [0, 1],
                "bounds": ["0", "1"]
            },
            "y_label": "",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "hidden",
            "height": 250,
            "annot_options": {
                "annot_background_colors": [
                    {
                        "y0": 0,
                        "y1": 1,
                        "color": "#000000",
                        "opacity": 1
                    }
                ]
            },
            "margins": {
                "top": 30,
                "bottom": 30
            },
            "plot_elements": [
                {
                    "title": "SDO HMIB",
                    "visible": true,
                    "cadence": "PT2H",
                    "plot_options": {
                        "plot_type": "url_template_epoch_images",
                        "url_template": "https://sdo.gsfc.nasa.gov/assets/img/browse/{YYYY}/{MM}/{DD}/{YYYYMMDD}_{HHmmss}_256_HMIBC.jpg",
                        "epoch_offset_start_of_day": "PT0M",
                        "time_substitutions": [
                            {
                                "placeholder": "{YYYY}",
                                "format": "YYYY",
                                "epoch_offset": "PT0M"
                            },
                            {
                                "placeholder": "{MM}",
                                "format": "MM",
                                "epoch_offset": "PT0M"
                            },
                            {
                                "placeholder": "{DD}",
                                "format": "DD",
                                "epoch_offset": "PT0M"
                            },
                            {
                                "placeholder": "{YYYYMMDD}",
                                "format": "YYYYMMDD",
                                "epoch_offset": "PT0M"
                            },
                            {
                                "placeholder": "{HHmmss}",
                                "format": "HHmmss",
                                "epoch_offset": "PT0M"
                            }
                        ],
                        "epoch_indicator_color": "yellow"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "",
                        "parameter": "",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {}
                }
            ]
        },
        {
            "title": "GOES X-ray flux",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": [-9, -2],
                "bounds": ["-9", "-2"]
            },
            "y_label": "X-ray flux (W/m2)",
            "scale": "log10",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "height": 80,
            "annot_options": {
                "annot_horizontal_grid_lines": [
                    {
                        "y": 1e-6
                    },
                    {
                        "y": 1e-5
                    },
                    {
                        "y": 0.0001
                    },
                    {
                        "y": 0.001
                    }
                ],
                "annot_text_items": [
                    {
                        "y": 3.163e-6,
                        "align": "left",
                        "text": "C"
                    },
                    {
                        "y": 3.163e-5,
                        "align": "left",
                        "text": "M"
                    },
                    {
                        "y": 0.0003163,
                        "align": "left",
                        "text": "X"
                    }
                ]
            },
            "margins": {
                "top": 30,
                "bottom": 50
            },
            "plot_elements": [
                {
                    "title": "Long X-rays (1-8 Å)",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#8f9cbc",
                        "stroke-width": "2",
                        "stroke-opacity": 1,
                        "fill": "#b5c7f2",
                        "fill-opacity": 0.52,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "xray_flux_rt",
                        "parameter": "xray_flux_long",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Short X-rays (0.5-4 Å)",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#7c8498",
                        "stroke-width": 0,
                        "stroke-opacity": 1,
                        "fill": "#b8c5e5",
                        "fill-opacity": 1,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "xray_flux_rt",
                        "parameter": "xray_flux_short",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        }
    ]
}
