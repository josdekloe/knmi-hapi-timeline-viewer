{
    "options": {
        "fill_vertical": true,
        "fill_horizontal": true,
        "zoom_detail_factor": 1
    },
    "default_zoom": {
        "tmin": "-P3M",
        "tmax": "P4D"
    },
    "global_margins": {
        "top": 0,
        "bottom": 0,
        "left": 65,
        "right": 30
    },
    "subplots": [
        {
            "title": "Swarm A and C electron density",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": [0, 5.12],
                "bounds": ["0", "6"]
            },
            "y_label": "Ne (10⁶/cm³)",
            "scale": "linear",
            "ytick_format": ".1f",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 300,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 20,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "title": "Swarm C max Ne",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "area",
                        "baseline_mode": "constant",
                        "baseline": 0,
                        "stroke": "#008cb4",
                        "stroke-width": "1",
                        "stroke-opacity": "1",
                        "fill": "#caf0fe",
                        "fill-opacity": 0.13,
                        "curve_type": "natural"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "ipir_ne_swarmc_minmaxmean_PT1H40M",
                        "parameter": "Ne_max",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm A Ne",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#b51a00",
                        "stroke-width": 2,
                        "stroke-opacity": "1",
                        "fill": "#AAAAAA",
                        "fill-opacity": "1",
                        "curve_type": "linear",
                        "baseline_mode": "constant"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_efia_lp",
                        "parameter": "Ne",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C Ne",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "baseline_mode": "constant",
                        "baseline": 0,
                        "stroke": "#008cb4",
                        "stroke-width": 2,
                        "stroke-opacity": "1",
                        "fill": "#AAAAAA",
                        "fill-opacity": "1",
                        "curve_type": "linear"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_efic_lp",
                        "parameter": "Ne",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "WAM-IPE Ne for Swarm C",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#74a7fe",
                        "stroke-width": 3,
                        "stroke-opacity": 0.41,
                        "fill": "#AAAAAA",
                        "fill-opacity": "1",
                        "curve_type": "linear"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "wam_ipe_ne_swarmc_PT10S",
                        "parameter": "electron_density",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "Swarm C neutral density",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": [0, 3.98],
                "bounds": ["0", "6"]
            },
            "y_label": "density (10⁻¹² kg/m³)",
            "scale": "linear",
            "ytick_format": ".1f",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "height": 300,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 10,
                "bottom": 30
            },
            "plot_elements": [
                {
                    "title": "Swarm C neutral density min/max",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "area",
                        "baseline_mode": "parameter",
                        "baseline": 0,
                        "stroke": "#008cb4",
                        "stroke-width": 1,
                        "stroke-opacity": "1",
                        "fill": "#caf0fe",
                        "fill-opacity": 0.23,
                        "curve_type": "natural"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "thermosphere_swarmc_minmaxmean_PT1H40M",
                        "parameter": "density_max",
                        "time_parameter": "time",
                        "baseline_parameter": "density_min",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1000000000000,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C neutral density",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "baseline_mode": "constant",
                        "baseline": 0,
                        "stroke": "#008cb4",
                        "stroke-width": 1,
                        "stroke-opacity": "1",
                        "fill": "#AAAAAA",
                        "fill-opacity": "1",
                        "curve_type": "linear"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "thermosphere_swarmc_PT30S",
                        "parameter": "density",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1000000000000,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C ACC neutral density",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#038cb4",
                        "stroke-width": 2,
                        "stroke-opacity": "1",
                        "fill": "#AAAAAA",
                        "fill-opacity": "1",
                        "curve_type": "linear"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "thermosphere_swarmc_acc_PT10S",
                        "parameter": "density",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1000000000000,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "WAM-IPE Ne for Swarm C",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#74a7fe",
                        "stroke-width": 3,
                        "stroke-opacity": 0.48,
                        "fill": "#AAAAAA",
                        "fill-opacity": "1",
                        "curve_type": "linear"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "wam_ipe_neutral_swarmc_PT10S",
                        "parameter": "mass_density",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 500000000000,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "Interplanetary Magnetic Field at Earth",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": ["-30", "30"],
                "bounds": ["1", "50"],
                "step": 1,
                "mirror": true
            },
            "scale": "linear",
            "ytick_format": "d",
            "y_label": "IMF (nT)",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 280,
            "margins": {
                "top": 10,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "title": "IMF Bt - realtime",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "area",
                        "baseline_mode": "mirror_zero_x",
                        "curve_type": "linear",
                        "stroke": "#000000",
                        "stroke-width": "0",
                        "stroke-opacity": "1",
                        "fill": "#d6d6d6",
                        "fill-opacity": 1
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "solar_wind_mag_rt",
                        "parameter": "bt",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "IMF GSM Bz - realtime",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "area",
                        "baseline_mode": "constant",
                        "baseline": 0,
                        "parameter_multplier": 1,
                        "curve_type": "linear",
                        "stroke": "#000000",
                        "stroke-width": "0",
                        "stroke-opacity": "1",
                        "fill": "#C4472E",
                        "fill-opacity": "1"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "solar_wind_mag_rt",
                        "parameter": "bz_gsm",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "IMF GSM By - realtime",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#008ce6",
                        "stroke-width": "1",
                        "stroke-opacity": "1",
                        "fill": "#AAAAAA",
                        "fill-opacity": "1",
                        "curve_type": "linear"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "solar_wind_mag_rt",
                        "parameter": "by_gsm",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "IMF Bt - ACE science data",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#c0c0c0",
                        "stroke-width": 1,
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": 0,
                        "curve_type": "linear",
                        "baseline_mode": "mirror_zero_x",
                        "baseline": 0.08
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "solar_wind_mag_ace",
                        "parameter": "bt",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "IMF Bz - ACE science data",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#b51a00",
                        "stroke-width": "1",
                        "stroke-opacity": "1",
                        "fill": "#b51a00",
                        "fill-opacity": 0.07,
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": 0
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "solar_wind_mag_ace",
                        "parameter": "bz_gsm",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "Hp30 index",
            "visible": true,
            "legend_visible": false,
            "ydomain": {
                "default": [0, "9"],
                "bounds": ["0", "15"]
            },
            "y_label": "",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 80,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 10,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "title": "Hp30 index",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "bars",
                        "stroke": "#000000",
                        "stroke-width": "0",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "curve_type": "linear",
                        "bar_offset_seconds": 1800,
                        "bar_padding": 0.2,
                        "bar_text": true,
                        "fill_colors": [
                            {
                                "threshold": 4.6,
                                "color": "#88CC66"
                            },
                            {
                                "threshold": 5.6,
                                "color": "#FFF033"
                            },
                            {
                                "threshold": 6.6,
                                "color": "#FFBB44"
                            },
                            {
                                "threshold": 7.6,
                                "color": "#DD9922"
                            },
                            {
                                "threshold": 8.6,
                                "color": "#FF2020"
                            },
                            {
                                "threshold": 9.6,
                                "color": "#A02020"
                            },
                            {
                                "threshold": 1000,
                                "color": "#300000"
                            }
                        ]
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "hp30_index",
                        "parameter": "Hp30",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {}
                }
            ]
        },
        {
            "title": "F10.7",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": [0, 350],
                "bounds": [0, 500],
                "step": 1,
                "mirror": false
            },
            "y_label": "F10.7 (SFU)",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "annot_options": {
                "annot_horizontal_grid_lines": [
                    {
                        "y": 100
                    },
                    {
                        "y": 200
                    }
                ],
                "annot_text_items": [
                    {
                        "y": 100,
                        "text": "100"
                    },
                    {
                        "y": 200,
                        "text": "200"
                    }
                ]
            },
            "height": 150,
            "margins": {
                "top": 10,
                "bottom": 50
            },
            "plot_elements": [
                {
                    "title": "F10.7 solar radio flux",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "area",
                        "baseline_mode": "constant",
                        "baseline": 0,
                        "curve_type": "step_after",
                        "stroke": "#aaaaaa",
                        "stroke-width": 2.79,
                        "stroke-opacity": 1,
                        "fill": "#ebebeb",
                        "fill-opacity": 1
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "f10_7",
                        "parameter": "f10_7",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        }
    ]
}
