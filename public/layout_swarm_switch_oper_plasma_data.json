{
    "options": {
        "fill_vertical": true,
        "fill_horizontal": true,
        "zoom_detail_factor": 1
    },
    "default_zoom": {
        "tmin": "-PT18H",
        "tmax": "PT3H"
    },
    "global_margins": {
        "top": 0,
        "bottom": 0,
        "left": 65,
        "right": 30
    },
    "subplots": [
        {
            "title": "Swarm orbits and orbit altitude",
            "visible": true,
            "legend_visible": false,
            "globe_visible": true,
            "ydomain": {
                "default": [400, 550],
                "bounds": [200, 600]
            },
            "y_label": "Altitude (km)",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "height": 130,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 20,
                "bottom": 10
            },
            "options_3d": {
                "camera_attached_to": "Sun",
                "x_position": 0.5,
                "render_earth": true
            },
            "plot_elements": [
                {
                    "title": "Swarm A orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#006CD9",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#006CD9",
                        "fill-opacity": 0.16,
                        "label": "A",
                        "label_position_x": 0.75,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_moda_sc_PT30S",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Swarm B orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#A070DC",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#A070DC",
                        "fill-opacity": 0.16,
                        "label": "B",
                        "label_position_x": 0.75,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modb_sc_PT30S",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Swarm C orbit",
                    "visible": false,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#74a7fe",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#74a7fe",
                        "fill-opacity": 0.16,
                        "label": "C",
                        "label_position_x": 0.25,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modc_sc_PT30S",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Swarm A altitude long-term min/max",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#006CD9",
                        "stroke-width": 0,
                        "stroke-opacity": "1",
                        "fill": "#006CD9",
                        "fill-opacity": 0.1,
                        "mirror_zero_x": false,
                        "curve_type": "step",
                        "baseline_mode": "parameter"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_moda_sc_aggregate",
                        "parameter": "height_max",
                        "time_parameter": "time",
                        "baseline_parameter": "height_min",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0.001,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm B altitude long-term min/max",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#A070DC",
                        "stroke-width": 0,
                        "stroke-opacity": "1",
                        "fill": "#A070DC",
                        "fill-opacity": 0.1,
                        "mirror_zero_x": false,
                        "curve_type": "step",
                        "baseline_mode": "parameter"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modb_sc_aggregate",
                        "parameter": "height_max",
                        "time_parameter": "time",
                        "baseline_parameter": "height_min",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0.001,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C altitude long-term min/max",
                    "visible": false,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#74a7fe",
                        "stroke-width": 0,
                        "stroke-opacity": "1",
                        "fill": "#74a7fe",
                        "fill-opacity": 0.1,
                        "mirror_zero_x": false,
                        "curve_type": "step",
                        "baseline_mode": "parameter"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modc_sc_aggregate",
                        "parameter": "height_max",
                        "time_parameter": "time",
                        "baseline_parameter": "height_min",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0.001,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm A altitude long-term mean",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#006CD9",
                        "stroke-width": 1,
                        "stroke-opacity": "1",
                        "fill": "#006CD9",
                        "fill-opacity": 0.1,
                        "mirror_zero_x": false,
                        "curve_type": "step",
                        "baseline_mode": "constant"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_moda_sc_aggregate",
                        "parameter": "height_mean",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0.001,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm B altitude long-term mean",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#A070DC",
                        "stroke-width": 1,
                        "stroke-opacity": "1",
                        "fill": "#A070DC",
                        "fill-opacity": 0.1,
                        "mirror_zero_x": false,
                        "curve_type": "step",
                        "baseline_mode": "constant"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modb_sc_aggregate",
                        "parameter": "height_mean",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0.001,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C altitude long-term mean",
                    "visible": false,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#74a7fe",
                        "stroke-width": 1,
                        "stroke-opacity": "1",
                        "fill": "#74a7fe",
                        "fill-opacity": 0.1,
                        "mirror_zero_x": false,
                        "curve_type": "step",
                        "baseline_mode": "constant"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modc_sc_aggregate",
                        "parameter": "height_mean",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0.001,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm A altitude",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#006CD9",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#006CD9",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_moda_sc",
                        "parameter": "height",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0.001,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Swarm B altitude",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#A070DC",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#A070DC",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modb_sc",
                        "parameter": "height",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0.001,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Swarm C altitude",
                    "visible": false,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#74a7fe",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#74a7fe",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modc_sc",
                        "parameter": "height",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0.001,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                }
            ]
        },
        {
            "title": "Swarm OPER electron density",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": [0, 3],
                "bounds": [0, 6]
            },
            "y_label": "Ne (10⁶/cm³)",
            "scale": "linear",
            "ytick_format": "auto",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 130,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 20,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "title": "Swarm A Ne long-term min/max",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#006CD9",
                        "stroke-width": 0,
                        "stroke-opacity": "1",
                        "fill": "#006CD9",
                        "fill-opacity": 0.1,
                        "mirror_zero_x": false,
                        "curve_type": "step",
                        "baseline_mode": "parameter"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_efia_lp_aggregate",
                        "parameter": "Ne_max",
                        "time_parameter": "time",
                        "baseline_parameter": "Ne_min",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm B Ne long-term min/max",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#A070DC",
                        "stroke-width": 0,
                        "stroke-opacity": "1",
                        "fill": "#A070DC",
                        "fill-opacity": 0.1,
                        "mirror_zero_x": false,
                        "curve_type": "step",
                        "baseline_mode": "parameter"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_efib_lp_aggregate",
                        "parameter": "Ne_max",
                        "time_parameter": "time",
                        "baseline_parameter": "Ne_min",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm A Ne long-term mean",
                    "visible": true,
                    "plot_options": {
                        "stroke": "#006CD9",
                        "stroke-width": 1,
                        "stroke-opacity": 1,
                        "fill": "#006CD9",
                        "fill-opacity": "1",
                        "mirror_zero_x": false,
                        "plot_type": "line",
                        "curve_type": "step",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_efia_lp_aggregate",
                        "parameter": "Ne_mean",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm B Ne long-term mean",
                    "visible": true,
                    "plot_options": {
                        "stroke": "#A070DC",
                        "stroke-width": 1,
                        "stroke-opacity": 1,
                        "fill": "#A070DC",
                        "fill-opacity": "1",
                        "mirror_zero_x": false,
                        "plot_type": "line",
                        "curve_type": "step",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_efib_lp_aggregate",
                        "parameter": "Ne_mean",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm A Ne 2Hz OPER data",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#006CD9",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#006CD9",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_efia_lp",
                        "parameter": "Ne",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm B Ne 2Hz OPER data",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#A070DC",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#A070DC",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_efib_lp",
                        "parameter": "Ne",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C Ne 2Hz OPER data",
                    "visible": false,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#70B8FF",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#70B8FF",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_efic_lp",
                        "parameter": "Ne",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "Swarm PREL external magnetic field (internal field contributions removed using CHAOS model)",
            "visible": false,
            "ydomain": {
                "default": [-200, 200],
                "bounds": [-2000, 2000],
                "mirror": true
            },
            "y_label": "Delta-B (nT)",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 130,
            "annot_options": {
                "annot_horizontal_grid_lines": [
                    {
                        "y": 0.0
                    }
                ],
                "annot_text_items": []
            },
            "margins": {
                "top": 10,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "title": "Delta-B Northward",
                    "visible": false,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#cc4444",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": 0
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_prel_maga_lr",
                        "parameter": "Delta_B_NEC_1",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Delta-B Eastward",
                    "visible": false,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#4f8856",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": 0
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_prel_maga_lr",
                        "parameter": "Delta_B_NEC_2",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Delta-B towards Earth centre",
                    "visible": false,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#006CD9",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#006CD9",
                        "fill-opacity": 0.17,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": 0
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_prel_maga_lr",
                        "parameter": "Delta_B_NEC_3",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm A Delta-B Eastward",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#006CD9",
                        "stroke-width": "2",
                        "stroke-opacity": 1,
                        "fill": "#006CD9",
                        "fill-opacity": 0.3,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": "mirror_zero_x"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_prel_maga_lr",
                        "parameter": "Delta_B_NEC_2",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm B Delta-B Eastward",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#A070DC",
                        "stroke-width": "2",
                        "stroke-opacity": 1,
                        "fill": "#A070DC",
                        "fill-opacity": 0.3,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": "mirror_zero_x"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_prel_magb_lr",
                        "parameter": "Delta_B_NEC_2",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C Delta-B Eastward",
                    "visible": false,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#70B8FF",
                        "stroke-width": "2",
                        "stroke-opacity": 1,
                        "fill": "#70B8FF",
                        "fill-opacity": 0.3,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": "mirror_zero_x"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_prel_magc_lr",
                        "parameter": "Delta_B_NEC_2",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ],
            "legend_visible": true
        },
        {
            "title": "Hp30 index",
            "visible": true,
            "ydomain": {
                "default": [0, "9"],
                "bounds": ["0", "15"]
            },
            "y_label": "Hp30 index",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "height": 50,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 10,
                "bottom": 40
            },
            "plot_elements": [
                {
                    "title": "Hp30 index",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "bars",
                        "stroke": "#000000",
                        "stroke-width": "0",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "curve_type": "linear",
                        "bar_offset_seconds": 1800,
                        "bar_padding": 0.2,
                        "bar_text": true,
                        "fill_colors": [
                            {
                                "threshold": 4.6,
                                "color": "#88CC66"
                            },
                            {
                                "threshold": 5.6,
                                "color": "#FFF033"
                            },
                            {
                                "threshold": 6.6,
                                "color": "#FFBB44"
                            },
                            {
                                "threshold": 7.6,
                                "color": "#DD9922"
                            },
                            {
                                "threshold": 8.6,
                                "color": "#FF2020"
                            },
                            {
                                "threshold": 9.6,
                                "color": "#A02020"
                            },
                            {
                                "threshold": 1000,
                                "color": "#300000"
                            }
                        ]
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "hp30_index",
                        "parameter": "Hp30",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {}
                }
            ],
            "legend_visible": true
        }
    ]
}
