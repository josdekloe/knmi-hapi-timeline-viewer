{
    "options": {
        "fill_vertical": true,
        "fill_horizontal": true,
        "zoom_detail_factor": 1
    },
    "default_zoom": {
        "tmin": "-P3D",
        "tmax": "P3D"
    },
    "global_margins": {
        "top": 0,
        "bottom": 0,
        "left": 85,
        "right": 30
    },
    "subplots": [
        {
            "title": "GOES X-ray flux",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": [-7, -2],
                "bounds": ["-9", "-2"]
            },
            "y_label": "X-ray flux (W/m²)",
            "scale": "log10",
            "ytick_format": "d",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 150,
            "annot_options": {
                "annot_horizontal_grid_lines": [
                    {
                        "y": 1e-6
                    },
                    {
                        "y": 1e-5
                    },
                    {
                        "y": 0.0001
                    },
                    {
                        "y": 0.001
                    }
                ],
                "annot_text_items": []
            },
            "margins": {
                "top": 30,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "title": "X-ray flux long",
                    "visible": true,
                    "clipped": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#8f9cbc",
                        "stroke-width": "2",
                        "stroke-opacity": 1,
                        "fill": "#b5c7f2",
                        "fill-opacity": 0.52,
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": "1e-12",
                        "baseline_stroke": "#000000",
                        "baseline_stroke-width": "0",
                        "baseline_stroke-opacity": "0"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "xray_flux_rt",
                        "parameter": "xray_flux_long",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "refetch_interval_seconds": 60,
                        "zoom_detail_factor": 2
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "X-ray flux short",
                    "visible": true,
                    "clipped": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#7c8498",
                        "stroke-width": 0,
                        "stroke-opacity": 1,
                        "fill": "#b8c5e5",
                        "fill-opacity": 1,
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": "1e-12",
                        "baseline_stroke": "#000000",
                        "baseline_stroke-width": "0",
                        "baseline_stroke-opacity": "0"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "xray_flux_rt",
                        "parameter": "xray_flux_short",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "refetch_interval_seconds": 60,
                        "zoom_detail_factor": 2
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Flare class",
                    "visible": true,
                    "clipped": false,
                    "include_in_legend": false,
                    "plot_options": {
                        "plot_type": "xy_text_annotation",
                        "text_source": "text_parameter",
                        "vertical_placement": "parameter",
                        "xshift": 0,
                        "yshift": 5,
                        "sort_by_y": "descending",
                        "prevent_overlap_x": 30,
                        "prevent_overlap_y": 15,
                        "font_size": 11,
                        "text_color": "#000000",
                        "text_opacity": 1,
                        "dominant_baseline": "auto",
                        "text_anchor": "middle",
                        "number_to_text_format": ".2e",
                        "convert_exponential_notation": true,
                        "text_prefix": "",
                        "text_suffix": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "xray_flux_flare_class",
                        "parameter": "xrsb_flux",
                        "text_parameter": "flare_class",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "off",
                        "refetch_interval_seconds": 3600,
                        "zoom_detail_factor": 4
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "GOES proton flux",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": [-2, 4],
                "bounds": ["-3", "6"]
            },
            "y_label": "Proton flux (pfu)",
            "scale": "log10",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "height": 150,
            "annot_options": {
                "annot_horizontal_grid_lines": [
                    {
                        "y": 10
                    }
                ],
                "annot_text_items": []
            },
            "margins": {
                "top": 10,
                "bottom": 40
            },
            "plot_elements": [
                {
                    "title": "> 10 MeV",
                    "visible": true,
                    "clipped": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#83afc2",
                        "stroke-width": 2,
                        "stroke-opacity": 1,
                        "fill": "#c6dae3",
                        "fill-opacity": 1,
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": "1e-12",
                        "baseline_stroke": "#000000",
                        "baseline_stroke-width": "0",
                        "baseline_stroke-opacity": "0"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "proton_flux_rt",
                        "parameter": "proton_flux_10MeV",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "refetch_interval_seconds": 60,
                        "zoom_detail_factor": 2
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "> 50 MeV",
                    "visible": true,
                    "clipped": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#000000",
                        "stroke-width": 0,
                        "stroke-opacity": 1,
                        "fill": "#9bbfce",
                        "fill-opacity": 1,
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": "1e-12",
                        "baseline_stroke": "#000000",
                        "baseline_stroke-width": "0",
                        "baseline_stroke-opacity": "0"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "proton_flux_rt",
                        "parameter": "proton_flux_50MeV",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "refetch_interval_seconds": 60,
                        "zoom_detail_factor": 2
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "> 100 MeV",
                    "visible": true,
                    "clipped": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#000000",
                        "stroke-width": 0,
                        "stroke-opacity": 1,
                        "fill": "#83afc2",
                        "fill-opacity": 1,
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": "1e-12",
                        "baseline_stroke": "#000000",
                        "baseline_stroke-width": "0",
                        "baseline_stroke-opacity": "0"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "proton_flux_rt",
                        "parameter": "proton_flux_100MeV",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "refetch_interval_seconds": 60,
                        "zoom_detail_factor": 2
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "Interplanetary Magnetic Field near Sun-Earth L1",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": ["-30", "30"],
                "bounds": ["-80", "80"],
                "step": 1,
                "mirror": true
            },
            "scale": "linear",
            "ytick_format": "d",
            "y_label": "IMF (nT)",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 150,
            "margins": {
                "top": 10,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "title": "IMF Bt",
                    "visible": true,
                    "clipped": true,
                    "plot_options": {
                        "plot_type": "area",
                        "baseline_mode": "mirror_zero_x",
                        "curve_type": "linear",
                        "stroke": "#000000",
                        "stroke-width": "0",
                        "stroke-opacity": "1",
                        "fill": "#d6d6d6",
                        "fill-opacity": 1
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "solar_wind_mag_rt",
                        "parameter": "bt",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 60,
                        "zoom_detail_factor": 2
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "IMF GSM Bz",
                    "visible": true,
                    "clipped": true,
                    "plot_options": {
                        "plot_type": "area",
                        "baseline_mode": "constant",
                        "baseline": 0,
                        "baseline_stroke": "#000000",
                        "baseline_stroke-width": "0",
                        "baseline_stroke-opacity": "0",
                        "parameter_multplier": 1,
                        "curve_type": "linear",
                        "stroke": "#000000",
                        "stroke-width": "0",
                        "stroke-opacity": "1",
                        "fill": "#C4472E",
                        "fill-opacity": "1"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "solar_wind_mag_rt",
                        "parameter": "bz_gsm",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 60,
                        "zoom_detail_factor": 2
                    },
                    "data_transforms": {
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "IMF GSM By",
                    "visible": true,
                    "clipped": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#008ce6",
                        "stroke-width": "1",
                        "stroke-opacity": "1",
                        "fill": "#AAAAAA",
                        "fill-opacity": "1",
                        "curve_type": "linear"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "solar_wind_mag_rt",
                        "parameter": "by_gsm",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 60,
                        "zoom_detail_factor": 2
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "ENLIL Bt",
                    "visible": true,
                    "clipped": true,
                    "plot_options": {
                        "plot_type": "area",
                        "baseline_mode": "mirror_zero_x",
                        "baseline_stroke": "black",
                        "baseline_stroke-width": "1",
                        "baseline_stroke-opacity": "1",
                        "stroke": "black",
                        "stroke-width": "1",
                        "stroke-opacity": "1",
                        "fill": "#C5D0D9A",
                        "fill-opacity": "0.05",
                        "curve_type": "linear"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "solar_wind_plasma_enlil_metoffice",
                        "parameter": "bt",
                        "time_parameter": "time",
                        "issue_time_parameter": "timetag_issue",
                        "cadence_alternative_datasets_mode": "off",
                        "refetch_interval_seconds": 3600,
                        "zoom_detail_factor": 0.5
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "Solar wind speed near Sun-Earth L1",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": [200, 1000],
                "bounds": ["200", "1000"]
            },
            "y_label": "Speed (km/s)",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 150,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 10,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "title": "Solar wind speed",
                    "visible": true,
                    "clipped": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#fec700",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#ffe4a8",
                        "fill-opacity": 1,
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": "0",
                        "baseline_stroke": "#000000",
                        "baseline_stroke-width": "0",
                        "baseline_stroke-opacity": "0"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "solar_wind_plasma_rt",
                        "parameter": "speed",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "refetch_interval_seconds": 60,
                        "zoom_detail_factor": 2
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "ENLIL speed",
                    "visible": true,
                    "clipped": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#fdc700",
                        "stroke-width": 1,
                        "stroke-opacity": "1",
                        "fill": "#fdc700",
                        "fill-opacity": 0.17,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": 0,
                        "baseline_stroke": "#000000",
                        "baseline_stroke-width": "0",
                        "baseline_stroke-opacity": "0"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "solar_wind_plasma_enlil_metoffice",
                        "parameter": "speed",
                        "time_parameter": "time",
                        "issue_time_parameter": "timetag_issue",
                        "cadence_alternative_datasets_mode": "off",
                        "parameter_multiplier": 1,
                        "refetch_interval_seconds": 3600,
                        "zoom_detail_factor": 0.5
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "Solar wind density near Sun-Earth L1",
            "visible": false,
            "ydomain": {
                "default": [-1, 2],
                "bounds": ["-1", "2"]
            },
            "y_label": "Density (1/cm³)",
            "scale": "log10",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "height": 150,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 10,
                "bottom": 40
            },
            "plot_elements": [
                {
                    "title": "Solar wind density",
                    "visible": true,
                    "clipped": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#00a3d7",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#caf0fe",
                        "fill-opacity": "1",
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": "1e-9",
                        "baseline_stroke": "#000000",
                        "baseline_stroke-width": "0",
                        "baseline_stroke-opacity": "0"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "solar_wind_plasma_rt",
                        "parameter": "density",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "refetch_interval_seconds": 60,
                        "zoom_detail_factor": 2
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "ENLIL density",
                    "visible": true,
                    "clipped": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#00a2d7",
                        "stroke-width": 1,
                        "stroke-opacity": "1",
                        "fill": "#00a3d7",
                        "fill-opacity": 0.09,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": "1e-9",
                        "baseline_stroke": "#000000",
                        "baseline_stroke-width": "0",
                        "baseline_stroke-opacity": "0"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "solar_wind_plasma_enlil_metoffice",
                        "parameter": "density",
                        "time_parameter": "time",
                        "issue_time_parameter": "timetag_issue",
                        "cadence_alternative_datasets_mode": "off",
                        "parameter_multiplier": 1,
                        "refetch_interval_seconds": 3600,
                        "zoom_detail_factor": 0.5
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ],
            "legend_visible": true
        },
        {
            "title": "GFZ-Potsdam Kp index and NOAA Kp forecast",
            "visible": true,
            "legend_visible": false,
            "ydomain": {
                "default": [0, "9"],
                "bounds": ["0", "15"]
            },
            "y_label": "",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "height": 75,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 10,
                "bottom": 50
            },
            "plot_elements": [
                {
                    "title": "Kp index",
                    "visible": true,
                    "clipped": true,
                    "plot_options": {
                        "plot_type": "bars",
                        "stroke": "#000000",
                        "stroke-width": "0",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "curve_type": "linear",
                        "bar_offset_seconds": 1800,
                        "bar_padding": 0.2,
                        "bar_text": true,
                        "fill_colors": [
                            {
                                "threshold": 4.6,
                                "color": "#88CC66"
                            },
                            {
                                "threshold": 5.6,
                                "color": "#FFF033"
                            },
                            {
                                "threshold": 6.6,
                                "color": "#FFBB44"
                            },
                            {
                                "threshold": 7.6,
                                "color": "#DD9922"
                            },
                            {
                                "threshold": 8.6,
                                "color": "#FF2020"
                            },
                            {
                                "threshold": 9.6,
                                "color": "#A02020"
                            },
                            {
                                "threshold": 1000,
                                "color": "#300000"
                            }
                        ]
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "kp_index",
                        "parameter": "Kp",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 600,
                        "zoom_detail_factor": 2
                    },
                    "data_transforms": {}
                },
                {
                    "title": "Hp30 index",
                    "visible": false,
                    "plot_options": {
                        "plot_type": "bars",
                        "stroke": "#000000",
                        "stroke-width": "0",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "curve_type": "linear",
                        "bar_offset_seconds": 1800,
                        "bar_padding": 0.2,
                        "bar_text": true,
                        "fill_colors": [
                            {
                                "threshold": 4.6,
                                "color": "#88CC66"
                            },
                            {
                                "threshold": 5.6,
                                "color": "#FFF033"
                            },
                            {
                                "threshold": 6.6,
                                "color": "#FFBB44"
                            },
                            {
                                "threshold": 7.6,
                                "color": "#DD9922"
                            },
                            {
                                "threshold": 8.6,
                                "color": "#FF2020"
                            },
                            {
                                "threshold": 9.6,
                                "color": "#A02020"
                            },
                            {
                                "threshold": 1000,
                                "color": "#300000"
                            }
                        ]
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "hp30_index",
                        "parameter": "Hp30",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 600,
                        "zoom_detail_factor": 2
                    },
                    "data_transforms": {}
                },
                {
                    "title": "NOAA 3-day Kp forecast",
                    "visible": true,
                    "clipped": true,
                    "plot_options": {
                        "plot_type": "bars",
                        "bar_offset_seconds": 1800,
                        "bar_padding": 0,
                        "bar_text": false,
                        "fill_colors": [
                            {
                                "threshold": 4.6,
                                "color": "#88CC66"
                            },
                            {
                                "threshold": 5.6,
                                "color": "#FFF033"
                            },
                            {
                                "threshold": 6.6,
                                "color": "#FFBB44"
                            },
                            {
                                "threshold": 7.6,
                                "color": "#DD9922"
                            },
                            {
                                "threshold": 8.6,
                                "color": "#FF2020"
                            },
                            {
                                "threshold": 9.6,
                                "color": "#A02020"
                            },
                            {
                                "threshold": 1000,
                                "color": "#300000"
                            }
                        ],
                        "fill-opacity": "30%",
                        "stroke-width": "1px",
                        "stroke_colors": [
                            {
                                "threshold": 1000,
                                "color": "#888888"
                            }
                        ]
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "kp_index_noaa_geomag_forecast",
                        "parameter": "kp_index",
                        "time_parameter": "time",
                        "issue_time_parameter": "timetag_issue",
                        "cadence_alternative_datasets_mode": "off",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 0.5
                    },
                    "data_transforms": {}
                }
            ]
        }
    ]
}
