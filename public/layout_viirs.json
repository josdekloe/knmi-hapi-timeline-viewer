{
    "options": {
        "fill_vertical": true,
        "fill_horizontal": true,
        "zoom_detail_factor": 1
    },
    "default_zoom": {
        "tmin": "2024-05-09T12:00:00Z",
        "tmax": "2024-05-12T00:00:00Z"
    },
    "global_margins": {
        "top": 0,
        "bottom": 0,
        "left": 65,
        "right": 30
    },
    "subplots": [
        {
            "title": "VIIRS DNB North",
            "visible": true,
            "ydomain": {
                "default": [-350, 350],
                "bounds": [-500, 500]
            },
            "y_label": "",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "hidden",
            "height": 500,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": [],
                "annot_background_colors": [
                    {
                        "y0": -500,
                        "y1": 500,
                        "color": "#000000"
                    }
                ]
            },
            "margins": {
                "top": 20,
                "bottom": 20
            },
            "plot_elements": [
                {
                    "title": "VIIRS DNB images North",
                    "visible": true,
                    "plot_options": {
                        "max_time_diff_before_seconds": 10800,
                        "max_time_diff_after_seconds": 0,
                        "max_retain_images": 1,
                        "image_margins": {
                            "top": -100,
                            "bottom": 10
                        },
                        "marker_color": "#ffffff",
                        "aspect_ratio": 2,
                        "color_map": "Turbo",
                        "color_map_domain": {
                            "bounds": [0, 500],
                            "trim": [0, 500]
                        },
                        "reverse_color_map": false,
                        "symbol_type": "circle",
                        "symbol_height": 5,
                        "symbol_width": 5,
                        "plot_type": "epoch_image",
                        "stroke": "#000000",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "viirs_dnb_nh",
                        "parameter": "url",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "off",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 0.15
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "color_parameter_multiplier": 1,
                        "color_parameter_offset": 0,
                        "color_parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "VIIRS DNB South",
            "visible": true,
            "ydomain": {
                "default": [-350, 350],
                "bounds": [-500, 500]
            },
            "y_label": "",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "hidden",
            "height": 500,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": [],
                "annot_background_colors": [
                    {
                        "y0": -500,
                        "y1": 500,
                        "color": "#000000"
                    }
                ]
            },
            "margins": {
                "top": 20,
                "bottom": 30
            },
            "plot_elements": [
                {
                    "title": "VIIRS DNB images South",
                    "visible": true,
                    "plot_options": {
                        "max_time_diff_before_seconds": 10800,
                        "max_time_diff_after_seconds": 0,
                        "max_retain_images": 1,
                        "image_margins": {
                            "top": -100,
                            "bottom": 10
                        },
                        "marker_color": "#ffffff",
                        "aspect_ratio": 2,
                        "color_map": "Turbo",
                        "color_map_domain": {
                            "bounds": [0, 500],
                            "trim": [0, 500]
                        },
                        "reverse_color_map": false,
                        "symbol_type": "circle",
                        "symbol_height": 5,
                        "symbol_width": 5,
                        "plot_type": "epoch_image",
                        "stroke": "#000000",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "viirs_dnb_sh",
                        "parameter": "url",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "off",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 0.15
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "color_parameter_multiplier": 1,
                        "color_parameter_offset": 0,
                        "color_parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "Swarm A/B Eastward component of external magnetic field contributions",
            "visible": false,
            "ydomain": {
                "default": [-800, 800],
                "bounds": [-2000, 2000],
                "mirror": true
            },
            "y_label": "Delta-B (nT)",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 130,
            "annot_options": {
                "annot_horizontal_grid_lines": [
                    {
                        "y": 0.0
                    }
                ],
                "annot_text_items": []
            },
            "margins": {
                "top": 10,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "title": "Swarm A FAST Delta-B Eastward",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#006CD9",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#006CD9",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": 0
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_fast_maga_lr",
                        "parameter": "Delta_B_NEC_2",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm B FAST Delta-B Eastward",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#A070DC",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#A070DC",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": 0
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_fast_magb_lr",
                        "parameter": "Delta_B_NEC_2",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ],
            "legend_visible": true
        },
        {
            "title": "Auroral electroject Intermagnet stations",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": [-3500, 1500],
                "bounds": ["-5000", "5000"]
            },
            "y_label": "B (nT)",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "height": 150,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 30,
                "bottom": 40
            },
            "plot_elements": [
                {
                    "title": "Supermag SME U/L indices",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#000000",
                        "stroke-width": 0,
                        "stroke-opacity": 0,
                        "fill": "#888888",
                        "fill-opacity": 0.3,
                        "curve_type": "linear",
                        "baseline_mode": "parameter"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "supermag_sme_index",
                        "parameter": "smu",
                        "time_parameter": "time",
                        "baseline_parameter": "sml",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 4.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Abisko",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#2ca02c",
                        "stroke-width": 1,
                        "stroke-opacity": "1",
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://imag-data.bgs.ac.uk/GIN_V1/hapi",
                        "dataset": "abk/best-avail/PT1M/xyzf",
                        "parameter": "Field_Vector",
                        "parameter_component_index": 0,
                        "time_parameter": "Time",
                        "cadence_alternative_datasets_mode": "intermagnet",
                        "refetch_interval_seconds": 300,
                        "zoom_detail_factor": 4
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": true
                    }
                },
                {
                    "title": "Barrow",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#98df8a",
                        "stroke-width": 1,
                        "stroke-opacity": "1",
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://imag-data.bgs.ac.uk/GIN_V1/hapi",
                        "dataset": "brw/best-avail/PT1M/xyzf",
                        "parameter": "Field_Vector",
                        "parameter_component_index": 0,
                        "time_parameter": "Time",
                        "cadence_alternative_datasets_mode": "intermagnet",
                        "refetch_interval_seconds": 300,
                        "zoom_detail_factor": 4
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": true
                    }
                },
                {
                    "title": "College",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#c49c94",
                        "stroke-width": 1,
                        "stroke-opacity": "1",
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://imag-data.bgs.ac.uk/GIN_V1/hapi",
                        "dataset": "cmo/best-avail/PT1M/xyzf",
                        "parameter": "Field_Vector",
                        "parameter_component_index": 0,
                        "time_parameter": "Time",
                        "cadence_alternative_datasets_mode": "intermagnet",
                        "refetch_interval_seconds": 300,
                        "zoom_detail_factor": 4
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": true
                    }
                },
                {
                    "title": "Yellowknife",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#ff9896",
                        "stroke-width": 1,
                        "stroke-opacity": "1",
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://imag-data.bgs.ac.uk/GIN_V1/hapi",
                        "dataset": "ykc/best-avail/PT1M/xyzf",
                        "parameter": "Field_Vector",
                        "parameter_component_index": 0,
                        "time_parameter": "Time",
                        "cadence_alternative_datasets_mode": "intermagnet",
                        "refetch_interval_seconds": 300,
                        "zoom_detail_factor": 4
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": true
                    }
                },
                {
                    "title": "Fort Churchill",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#aec7e8",
                        "stroke-width": 1,
                        "stroke-opacity": 1,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://imag-data.bgs.ac.uk/GIN_V1/hapi",
                        "dataset": "fcc/best-avail/PT1M/xyzf",
                        "parameter": "Field_Vector",
                        "parameter_component_index": 0,
                        "time_parameter": "Time",
                        "cadence_alternative_datasets_mode": "intermagnet",
                        "refetch_interval_seconds": 300,
                        "zoom_detail_factor": 4
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": true
                    }
                },
                {
                    "title": "Narsarsuaq",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#ff7f0e",
                        "stroke-width": 1,
                        "stroke-opacity": "1",
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://imag-data.bgs.ac.uk/GIN_V1/hapi",
                        "dataset": "naq/best-avail/PT1M/xyzf",
                        "parameter": "Field_Vector",
                        "parameter_component_index": 0,
                        "time_parameter": "Time",
                        "cadence_alternative_datasets_mode": "intermagnet",
                        "refetch_interval_seconds": 300,
                        "zoom_detail_factor": 4
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": true
                    }
                },
                {
                    "title": "Sanikiluaq (Poste-de-la-Baleine)",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#d62728",
                        "stroke-width": 1,
                        "stroke-opacity": "1",
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://imag-data.bgs.ac.uk/GIN_V1/hapi",
                        "dataset": "pbq/best-avail/PT1M/xyzf",
                        "parameter": "Field_Vector",
                        "parameter_component_index": 0,
                        "time_parameter": "Time",
                        "cadence_alternative_datasets_mode": "intermagnet",
                        "refetch_interval_seconds": 300,
                        "zoom_detail_factor": 4
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": -400,
                        "parameter_offset_remove_mean": true
                    }
                }
            ]
        },
        {
            "title": "GFZ-Potsdam Hp30 index",
            "visible": true,
            "legend_visible": false,
            "ydomain": {
                "default": [0, "12"],
                "bounds": ["0", "15"]
            },
            "y_label": "",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "height": 80,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 10,
                "bottom": 40
            },
            "plot_elements": [
                {
                    "title": "Hp30 index",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "bars",
                        "stroke": "#000000",
                        "stroke-width": "0",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "curve_type": "linear",
                        "bar_offset_seconds": 1800,
                        "bar_padding": 0.2,
                        "bar_text": true,
                        "fill_colors": [
                            {
                                "threshold": 4.6,
                                "color": "#88CC66"
                            },
                            {
                                "threshold": 5.6,
                                "color": "#FFF033"
                            },
                            {
                                "threshold": 6.6,
                                "color": "#FFBB44"
                            },
                            {
                                "threshold": 7.6,
                                "color": "#DD9922"
                            },
                            {
                                "threshold": 8.6,
                                "color": "#FF2020"
                            },
                            {
                                "threshold": 9.6,
                                "color": "#A02020"
                            },
                            {
                                "threshold": 1000,
                                "color": "#300000"
                            }
                        ]
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "hp30_index",
                        "parameter": "Hp30",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 600,
                        "zoom_detail_factor": 2
                    },
                    "data_transforms": {}
                },
                {
                    "title": "NOAA 3-day Kp forecast",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "bars",
                        "bar_offset_seconds": 1800,
                        "bar_padding": 0,
                        "bar_text": false,
                        "fill_colors": [
                            {
                                "threshold": 4.6,
                                "color": "#88CC66"
                            },
                            {
                                "threshold": 5.6,
                                "color": "#FFF033"
                            },
                            {
                                "threshold": 6.6,
                                "color": "#FFBB44"
                            },
                            {
                                "threshold": 7.6,
                                "color": "#DD9922"
                            },
                            {
                                "threshold": 8.6,
                                "color": "#FF2020"
                            },
                            {
                                "threshold": 9.6,
                                "color": "#A02020"
                            },
                            {
                                "threshold": 1000,
                                "color": "#300000"
                            }
                        ],
                        "fill-opacity": "30%",
                        "stroke-width": "1px",
                        "stroke_colors": [
                            {
                                "threshold": 1000,
                                "color": "#888888"
                            }
                        ]
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "kp_index_noaa_geomag_forecast",
                        "parameter": "kp_index",
                        "time_parameter": "time",
                        "issue_time_parameter": "timetag_issue",
                        "cadence_alternative_datasets_mode": "off",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 0.5
                    },
                    "data_transforms": {}
                }
            ]
        }
    ]
}
