{
    "options": {
        "fill_vertical": true,
        "fill_horizontal": true,
        "zoom_detail_factor": 1
    },
    "default_zoom": {
        "tmin": "2022-01-15T07:15:00.000Z",
        "tmax": "2022-01-15T08:40:00.000Z"
    },
    "global_margins": {
        "top": 0,
        "bottom": 0,
        "left": 65,
        "right": 30
    },
    "subplots": [
        {
            "title": "Swarm orbits and orbit altitude",
            "visible": false,
            "legend_visible": false,
            "globe_visible": true,
            "ydomain": {
                "default": [400, 550],
                "bounds": [200, 600]
            },
            "y_label": "Altitude (km)",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "height": 130,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 20,
                "bottom": 10
            },
            "options_3d": {
                "camera_attached_to": "Sun",
                "x_position": 0.5,
                "render_earth": true
            },
            "plot_elements": [
                {
                    "title": "Swarm A orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#006CD9",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#006CD9",
                        "fill-opacity": 0.16,
                        "label": "A",
                        "label_position_x": 0.75,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_moda_sc_PT30S",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Swarm B orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#A070DC",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#A070DC",
                        "fill-opacity": 0.16,
                        "label": "B",
                        "label_position_x": 0.75,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modb_sc_PT30S",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Swarm C orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#74a7fe",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#74a7fe",
                        "fill-opacity": 0.16,
                        "label": "C",
                        "label_position_x": 0.25,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modc_sc_PT30S",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Swarm C altitude long-term min/max",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#74a7fe",
                        "stroke-width": 0,
                        "stroke-opacity": "1",
                        "fill": "#74a7fe",
                        "fill-opacity": 0.1,
                        "mirror_zero_x": false,
                        "curve_type": "step",
                        "baseline_mode": "parameter"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modc_sc_aggregate",
                        "parameter": "height_max",
                        "time_parameter": "time",
                        "baseline_parameter": "height_min",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0.001,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C altitude long-term mean",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#74a7fe",
                        "stroke-width": 1,
                        "stroke-opacity": "1",
                        "fill": "#74a7fe",
                        "fill-opacity": 0.1,
                        "mirror_zero_x": false,
                        "curve_type": "step",
                        "baseline_mode": "constant"
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modc_sc_aggregate",
                        "parameter": "height_mean",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0.001,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C altitude",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#74a7fe",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#74a7fe",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modc_sc",
                        "parameter": "height",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0.001,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                }
            ]
        },
        {
            "title": "WACCM-X images",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": [-350, 350],
                "bounds": [-500, 500]
            },
            "y_label": "",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "hidden",
            "height": 250,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 10,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "title": "WACCM-X atomic oxygen mixing ratio",
                    "visible": true,
                    "plot_options": {
                        "max_time_diff_before_seconds": 900,
                        "max_time_diff_after_seconds": 900,
                        "max_retain_images": 1,
                        "aspect_ratio": 2,
                        "image_margins": {
                            "top": 10,
                            "bottom": 10
                        },
                        "marker_color": "#000000",
                        "color_map": "Greens",
                        "color_map_domain": {
                            "bounds": [0.93, 1.0],
                            "trim": [0.93, 1.0]
                        },
                        "reverse_color_map": false,
                        "symbol_type": "circle",
                        "symbol_height": 5,
                        "symbol_width": 5,
                        "plot_type": "epoch_image",
                        "stroke": "#000000",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "tonga_waccmx_images",
                        "parameter": "url",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 0.3
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "color_parameter_multiplier": 1,
                        "color_parameter_offset": 0,
                        "color_parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "HIAMCM images",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": [-350, 350],
                "bounds": [-500, 500]
            },
            "y_label": "",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "hidden",
            "height": 250,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 10,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "title": "HIAMCM neutral density (10⁻¹² kg/m³)",
                    "visible": true,
                    "plot_options": {
                        "max_time_diff_before_seconds": 900,
                        "max_time_diff_after_seconds": 900,
                        "max_retain_images": 1,
                        "aspect_ratio": 2,
                        "image_margins": {
                            "top": 10,
                            "bottom": 10
                        },
                        "marker_color": "#000000",
                        "color_map": "Blues",
                        "color_map_domain": {
                            "bounds": [0.9, 1.4],
                            "trim": [0.9, 1.4]
                        },
                        "reverse_color_map": false,
                        "symbol_type": "circle",
                        "symbol_height": 5,
                        "symbol_width": 5,
                        "plot_type": "epoch_image",
                        "stroke": "#000000",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "tonga_hiamcm_images",
                        "parameter": "url",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 0.3
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "color_parameter_multiplier": 1,
                        "color_parameter_offset": 0,
                        "color_parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "SAMI-3 images",
            "visible": false,
            "ydomain": {
                "default": [-350, 350],
                "bounds": [-500, 500]
            },
            "y_label": "",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "hidden",
            "height": 500,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 10,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "title": "SAMI-3 images",
                    "visible": true,
                    "plot_options": {
                        "aspect_ratio": 2,
                        "image_margins": {
                            "top": 10,
                            "bottom": 10
                        },
                        "marker_color": "#000000",
                        "color_map": "Greens",
                        "color_map_domain": {
                            "bounds": [0, 500],
                            "trim": [0, 500]
                        },
                        "reverse_color_map": false,
                        "symbol_type": "circle",
                        "symbol_height": 5,
                        "symbol_width": 5,
                        "plot_type": "epoch_image",
                        "stroke": "#000000",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "tonga_sami3_images",
                        "parameter": "url",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 0.3
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "color_parameter_multiplier": 1,
                        "color_parameter_offset": 0,
                        "color_parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "Swarm C neutral density",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": [0, 3],
                "bounds": [0, 6]
            },
            "y_label": "density (g/km³)",
            "scale": "linear",
            "ytick_format": "auto",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "height": 230,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 20,
                "bottom": 40
            },
            "plot_elements": [
                {
                    "title": "WACCM-X at Swarm C",
                    "visible": true,
                    "plot_options": {
                        "stroke": "#3ea85b",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#3ea85b",
                        "fill-opacity": 0,
                        "mirror_zero_x": false,
                        "plot_type": "line",
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "tonga_swarm_waccmx",
                        "parameter": "model_dens",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1000000000000,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "HIAMCM at Swarm C",
                    "visible": true,
                    "plot_options": {
                        "stroke": "#2976b8",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#2976b8",
                        "fill-opacity": 0,
                        "mirror_zero_x": false,
                        "plot_type": "line",
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "tonga_swarm_hiamcm",
                        "parameter": "model_dens",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1000000000000,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C GPS-derived density",
                    "visible": true,
                    "plot_options": {
                        "stroke": "#131517",
                        "stroke-width": "3",
                        "stroke-opacity": "1",
                        "fill": "#131517",
                        "fill-opacity": 0,
                        "mirror_zero_x": false,
                        "plot_type": "line",
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_dnscpod",
                        "parameter": "density",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1000000000000,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "GRACE-C neutral density",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": [0, 1],
                "bounds": [0, 6]
            },
            "y_label": "density (g/km³)",
            "scale": "linear",
            "ytick_format": "auto",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "height": 230,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 20,
                "bottom": 40
            },
            "plot_elements": [
                {
                    "title": "WACCM-X at GRACE-C",
                    "visible": true,
                    "plot_options": {
                        "stroke": "#3ea85b",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#3ea85b",
                        "fill-opacity": 0,
                        "mirror_zero_x": false,
                        "plot_type": "line",
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "tonga_grace_waccmx",
                        "parameter": "model_dens",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1000000000000,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "HIAMCM at GRACE-C",
                    "visible": true,
                    "plot_options": {
                        "stroke": "#2976b8",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#2976b8",
                        "fill-opacity": 0,
                        "mirror_zero_x": false,
                        "plot_type": "line",
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "tonga_grace_hiamcm",
                        "parameter": "model_dens",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1000000000000,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "GRACE-C accelerometer-derived density",
                    "visible": true,
                    "plot_options": {
                        "stroke": "#d2691e",
                        "stroke-width": "3",
                        "stroke-opacity": "1",
                        "fill": "#d2691e",
                        "fill-opacity": 0,
                        "mirror_zero_x": false,
                        "plot_type": "line",
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "tonga_grace_waccmx",
                        "parameter": "grace_dens",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1000000000000,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "Hp30 index",
            "visible": false,
            "ydomain": {
                "default": [0, "9"],
                "bounds": ["0", "15"]
            },
            "y_label": "Hp30 index",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "height": 50,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 10,
                "bottom": 30
            },
            "plot_elements": [
                {
                    "title": "Hp30 index",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "bars",
                        "stroke": "#000000",
                        "stroke-width": "0",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "curve_type": "linear",
                        "bar_offset_seconds": 1800,
                        "bar_padding": 0.2,
                        "bar_text": true,
                        "fill_colors": [
                            {
                                "threshold": 4.6,
                                "color": "#88CC66"
                            },
                            {
                                "threshold": 5.6,
                                "color": "#FFF033"
                            },
                            {
                                "threshold": 6.6,
                                "color": "#FFBB44"
                            },
                            {
                                "threshold": 7.6,
                                "color": "#DD9922"
                            },
                            {
                                "threshold": 8.6,
                                "color": "#FF2020"
                            },
                            {
                                "threshold": 9.6,
                                "color": "#A02020"
                            },
                            {
                                "threshold": 1000,
                                "color": "#300000"
                            }
                        ]
                    },
                    "data": {
                        "server": "https://data.spaceweather.knmi.nl/hapi",
                        "dataset": "hp30_index",
                        "parameter": "Hp30",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {}
                }
            ],
            "legend_visible": false
        }
    ]
}
