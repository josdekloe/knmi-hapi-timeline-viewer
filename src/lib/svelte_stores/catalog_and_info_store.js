import { writable } from "svelte/store";
import { useQueries } from "@sveltestack/svelte-query";
import { fetch_json } from "../fetch_json.js";
import { uid } from "../svelte_stores/uid_store.js";
import { get } from "svelte/store";

// export const catalogs = writable({});
// export const infos = writable({});
//
// export const get_catalogs = function(layout) {
//     let servers = new Set();
//     for (const subplot of layout.subplots) {
//         for (const plot_element of subplot.plot_elements) {
//             servers.add(plot_element.data.server);
//         }
//     }
//     let catalog_queries = useQueries(
//         [...servers].map(server => {
//             return {
//                 queryKey: ['catalog', server + '/catalog'],
//                 queryFn: fetch_json,
//                 staleTime: 86400e3,
//             }
//         })
//     )
//     return [[...servers], catalog_queries];
// }
//
// export const get_dataset_infos = function(layout) {
//     let info_urls = new Set();
//     for (const subplot of layout.data.subplots) {
//         for (const plot_element of subplot.plot_elements) {
//             info_urls.add(plot_element.data.server + '/info?id=' + plot_element.data.dataset);
//         }
//     }
//     let info_queries = useQueries(
//         [...info_urls].map(info_url => {
//         return {
//                 queryKey: ['info', info_url],
//                 queryFn: fetch_json,
//                 staleTime: 86400e3,
//             }
//         })
//     )
//     return [[...info_urls], info_queries];
// }

export const parameter_name = function (plot_element, parameter_key) {
    if (parameter_key + "_component_index" in plot_element.data) {
        return (
            plot_element.data[parameter_key] +
            "_" +
            plot_element.data[parameter_key + "_component_index"]
        );
    } else {
        return plot_element.data[parameter_key];
    }
};

const secondary_parameters = [
    "issue_time_parameter",
    "stop_time_parameter",
    "baseline_parameter",
    "color_map_parameter",
    "text_parameter",
    "x_coordinate_parameter",
    "y_coordinate_parameter",
    "z_coordinate_parameter",
];

export const collect_dataset_parameters = function (my_layout) {
    let unsorted_parameters = {};
    for (const subplot of my_layout.subplots) {
        for (const plot_element of subplot.plot_elements) {
            let url =
                plot_element.data.server +
                "/info?id=" +
                plot_element.data.dataset;
            if (!(url in unsorted_parameters)) {
                // First encounter of this dataset, create the set of parameters
                unsorted_parameters[url] = new Set();
                unsorted_parameters[url].add(plot_element.data.parameter);
            } else {
                // Dataset already encountered before, add additional main parameters
                unsorted_parameters[url].add(plot_element.data.parameter);
            }
            // Add secondary parameters, if available
            for (const par_field of secondary_parameters) {
                if (par_field in plot_element.data) {
                    if (!(par_field in unsorted_parameters[url])) {
                        // Add to the main list of parameters to download
                        unsorted_parameters[url].add(
                            plot_element.data[par_field],
                        );
                    }
                }
            }
            // Add field parameters, if available
            // if ("filters" in plot_element) {
            //     for (const par_field of plot_element.filters.map(f=>f.filter_parameter)) {
            //         if (!(par_field in unsorted_parameters[url])) {
            //             // Add to the main list of parameters to download
            //             unsorted_parameters[url].add(par_field);
            //         }
            //     }
            // }
        }
    }
    return unsorted_parameters;
};

export const set_dataset_parameters = function (my_layout) {
    let unsorted_parameters_list = collect_dataset_parameters(my_layout);
    for (const subplot of my_layout.subplots) {
        for (const plot_element of subplot.plot_elements) {
            if (!("computed" in plot_element)) {
                plot_element["computed"] = {};
            }
            const url =
                plot_element.data["server"] +
                "/info?id=" +
                plot_element.data["dataset"];
            plot_element["computed"]["unsorted_parameters"] =
                unsorted_parameters_list[url];
        }
    }
    return my_layout;
};

export const set_ids_on_newly_loaded_layout = function (my_layout) {
    for (const subplot of my_layout.subplots) {
        subplot["computed"] = {};
        uid.update((uidval) => uidval + 1);
        subplot["computed"]["id"] = get(uid);
        for (const plot_element of subplot.plot_elements) {
            plot_element["computed"] = {};
            uid.update((uidval) => uidval + 1);
            plot_element["computed"]["id"] = get(uid);
        }
    }
    return my_layout;
};
