import { writable } from "svelte/store";
export const global_zoom_detail_factor = writable(1.0);
export const range_pixels = writable(500.0);
