import { writable } from "svelte/store";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";
dayjs.extend(utc);
import {
    timeDomain,
    parse_time_or_duration_string,
} from "./timedomain_store.js";

let initial_layout = "/viewer/layout_real_time_space_weather.json";

let t0, t1;
// t0 = parse_time_or_duration_string($layoutQuery.data.default_zoom.tmin),
// t1 = parse_time_or_duration_string($layoutQuery.data.default_zoom.tmax);

let urlParams = new URLSearchParams(window.location.search);
if (urlParams.has("layout")) {
    initial_layout = "/viewer/layout_" + urlParams.get("layout") + ".json";
}

if (window.location.hash !== "") {
    let urlParams = new URLSearchParams(window.location.hash.substring(1));
    if (urlParams.has("layout")) {
        initial_layout = urlParams.get("layout");
    }
    if (urlParams.has("start")) {
        let start = urlParams.get("start");
        t0 = dayjs.utc(start);
    }
    if (urlParams.has("end")) {
        let end = urlParams.get("end");
        t1 = dayjs.utc(end);
    }
}

export const layout_file = writable(initial_layout);
export const layout = writable();
