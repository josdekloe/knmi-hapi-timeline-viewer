import { writable } from "svelte/store";
import dayjs from "dayjs";
import duration from "dayjs/plugin/duration";
import utc from "dayjs/plugin/utc";
dayjs.extend(duration);
dayjs.extend(utc);

export const timeDomain = writable([undefined, undefined]);
timeDomain.set([dayjs().startOf("year"), dayjs().endOf("year")]);

export const parse_time_or_duration_string = function (string) {
    let duration = dayjs.duration(string);
    let dayjsobj = dayjs.utc(string);
    if (dayjsobj.isValid()) {
    } else if (dayjs.isDuration(duration)) {
        if (string.startsWith("-")) {
            dayjsobj = dayjs().utc().subtract(duration);
        } else {
            dayjsobj = dayjs().utc().add(duration);
        }
    } else {
        console.log(`${string} could not be parsed`);
        dayjsobj = dayjs();
    }
    return dayjsobj;
};
