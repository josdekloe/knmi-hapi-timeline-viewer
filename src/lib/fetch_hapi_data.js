import { csvParseRows } from "d3";
// import { useQuery, useQueryClient } from '@sveltestack/svelte-query'

const hapi_list_parameter_names = function (hapi_info) {
    // Based on the HAPI info parameter object, returns a list
    // of parameter names. For multi-dimensional parameters, such as vectors,
    // the index is concatenated to the parameter name, so it can be treated as
    // a scalar parameter.
    return hapi_info.parameters
        .map(function (par) {
            let list = [];
            if ("size" in par) {
                for (let i = 0; i < par.size; i++) {
                    list.push(`${par.name}_${i}`);
                }
            } else {
                list.push(par.name);
            }
            return list;
        })
        .flat();
};

const reorganize_info = function (info_query, parameters) {
    // Put the HAPI dataset info on parameters into an object where the parameter name is the key,
    //   and the value is an object containing fill, size and type.
    let info_relevant = info_query.data.parameters.filter((p) =>
        parameters.has(p.name),
    );
    let info_reorganized = info_relevant.reduce(
        (obj, item) => (
            (obj[item.name] = {
                fill: item.fill === null ? NaN : item.fill,
                size: item.size,
                type: item.type,
            }),
            obj
        ),
        {},
    );
    return info_reorganized;
};

export const fetch_hapi_data = async function ({ queryKey }) {
    const [
        _key,
        server,
        dataset,
        cadence,
        info_query,
        parameters,
        time_domain,
    ] = queryKey;
    let datalist = [];
    // HAPI requirement:
    // Collect the parameters that the viewer needs in the same order as they
    //   are listed in the HAPI dataset info
    let all_parameters = info_query.data.parameters.map((par) => par.name);
    let req_parameters = all_parameters.filter((parname) =>
        parameters.has(parname),
    );

    // Create the URL
    let data_url =
        server +
        "/data?id=" +
        dataset +
        "&parameters=" +
        req_parameters.join(",") +
        "&time.min=" +
        time_domain[0].toISOString().slice(0, 19) +
        "Z" +
        "&time.max=" +
        time_domain[1].toISOString().slice(0, 19) +
        "Z";

    // Fetch the URL
    const controller = new AbortController();
    const signal = controller.signal;
    const response = await fetch(data_url, { signal });
    if (!response.ok) {
        throw new Error(
            "Network response was not ok on fetch of url: " + data_url,
        );
    }

    // Get the CSV contents
    let csv_text = await response.text();

    // Set up easy access to the dataset info for the required parameters
    let easy_info = reorganize_info(info_query, parameters);

    // Parse the CSV
    datalist = csvParseRows(csv_text, (csvcolumns, i) => {
        // Loop over the rows in the CSV file
        // and assign the values to their named parameters.
        // For instance, a CSV file that looks like this:
        // 2022-08-07T00:00:00Z,1
        // 2022-08-07T03:00:00Z,2
        // Will be converted to this:
        // [{time: '2022-08-07T00:00:00Z', kp_index: 1},
        //  {time: '2022-08-07T03:00:00Z', kp_index: 2]

        let obj = {};
        obj["time"] = new Date(csvcolumns[0]);
        let i_csv_column = 0;
        for (
            let i_parameter = 0;
            i_parameter < req_parameters.length;
            i_parameter++
        ) {
            let par = req_parameters[i_parameter];
            let parname = par;
            let size = easy_info[par]["size"] ? easy_info[par]["size"] : 1;
            let fill = easy_info[par]["fill"];
            let type = easy_info[par]["type"];

            for (let i_dimension = 0; i_dimension < size; i_dimension++) {
                i_csv_column++;
                if (size > 1) {
                    parname = par + "_" + i_dimension;
                }
                let columntext = csvcolumns[i_csv_column]; // Plus 1 because first timetag column must be skipped
                if (
                    columntext == "" ||
                    columntext == fill ||
                    +columntext == +fill
                ) {
                    obj[parname] = NaN;
                } else if (type == "isotime" || type == "string") {
                    // Do not parse isotime, but keep as text string here,
                    //   to enable exact comparison with selected forecast date later
                    obj[parname] = columntext;
                } else {
                    // Convert string to number
                    obj[parname] = Number(columntext);
                }
            }
        }
        return obj;
    });
    return {
        datalist: datalist,
        cadence: cadence,
        cancel: () => {
            console.log("Cancelling query");
            controller.abort();
        },
    };
};
